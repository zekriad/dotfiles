set -g default-terminal "tmux-256color"

set-option -g default-shell "/usr/bin/zsh"
# Automatically rename windows to the current directory
set-window-option -g automatic-rename on
set-window-option -g window-status-format ' #I #W '

set -s escape-time 1 # make tmux more responsive
set -g base-index 1 # 1-index windows
setw -g pane-base-index 1 # 1-index panes

set -g prefix C-t
# unbind C-b
# bind C-a send-prefix # Send to applications C-a when we hit it 2x

# bind c new-window -c "#{pane_current_path}" # default
bind c new-window -c "$HOME"
bind-key C-t last-window
bind r source-file ~/.tmux.conf \; display "Reloaded!" # reload tmux.conf

bind | split-window -h
bind - split-window -v

bind C-u resize-pane -U 5
bind C-d resize-pane -D 5
bind C-l resize-pane -L 5
bind C-p resize-pane -R 5

bind-key -T prefix C-n display-popup -E "nap"
bind-key -T prefix C-g command-prompt -p "Snippet Search:" "run-shell 'nap %% | tmux load-buffer -b tmp - ; tmux paste-buffer -p -b tmp -d'"
# bind-key -T prefix C-g command-prompt -p "Snippet Search:" "$SHELL --login -i -c 'nap %% | tmux load-buffer -b tmp - ; tmux paste-buffer -p -b tmp -d'"
# bind-key -T prefix C-g split-window \
  "$SHELL --login -i -c 'navi --print | tmux load-buffer -b tmp - ; tmux paste-buffer -p -t {last} -b tmp -d'"

set -g mouse on

### generic status bar
# set -g status-style fg=white # ,bg=black # status bar
# setw -g window-status-style fg=blue # ,bg=black
# setw -g window-status-current-style fg=cyan,bold # ,bg=black
#
# setw -g pane-border-style fg=blue # ,bg=black
# setw -g pane-active-border-style fg=cyan,bright # ,bg=black
#
# # setw -g window-style fg=colour240,bg=colour235
# # setw -g window-active-style fg=white,bg=black
#
# set -g message-style fg=white,bold,bg=black
#
# set -g status-left-length 40
# set -g status-left "#[fg=green]Session: #S #[fg=yellow]#I #[fg=cyan]#P #[fg=blue]::."
#
# set -g status-justify centre

### Plugins
# git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
# prefix-I to install
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @continuum-restore 'on'

set -g @plugin 'tmux-plugins/tmux-yank'
set -g @yank_action 'copy-pipe-no-clear'

set -g @plugin 'catppuccin/tmux#latest' # See https://github.com/catppuccin/tmux/tags for additional tags
set -g @catppuccin_flavor 'mocha' # latte, frappe, macchiato, or mocha

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'

### X clipboard
# These bindings are for X Windows only. If you're using a different
# window system you have to replace the `xsel` commands with something
# else. See https://github.com/tmux/tmux/wiki/Clipboard#available-tools
bind -T copy-mode    DoubleClick1Pane select-pane \; send -X select-word \; send -X copy-pipe-no-clear "xsel -i"
bind -T copy-mode-vi DoubleClick1Pane select-pane \; send -X select-word \; send -X copy-pipe-no-clear "xsel -i"
bind -n DoubleClick1Pane select-pane \; copy-mode -M \; send -X select-word \; send -X copy-pipe-no-clear "xsel -i"
bind -T copy-mode    TripleClick1Pane select-pane \; send -X select-line \; send -X copy-pipe-no-clear "xsel -i"
bind -T copy-mode-vi TripleClick1Pane select-pane \; send -X select-line \; send -X copy-pipe-no-clear "xsel -i"
bind -n TripleClick1Pane select-pane \; copy-mode -M \; send -X select-line \; send -X copy-pipe-no-clear "xsel -i"
bind -n MouseDown2Pane run "tmux set-buffer -b primary_selection \"$(xsel -o)\"; tmux paste-buffer -b primary_selection; tmux delete-buffer -b primary_selection"

bind -T copy-mode    C-c send -X copy-pipe-no-clear "xsel -i --clipboard"
bind -T copy-mode-vi C-c send -X copy-pipe-no-clear "xsel -i --clipboard"


# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'

