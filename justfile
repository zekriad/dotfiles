#!/usr/bin/env -S just --justfile

import? ".just_modules/macos.just"
import? ".just_modules/arch.just"
# import? ".just_modules/gentoo.just"

mod? eros ".just_modules/eros.just"

mod? things ".just_modules/things.just"
mod? tools ".just_modules/tools.just"

# List tasks
_default:
    @just --list

# Paste text for 24h
paste text expires_in="24":
    @echo {{ text }} | curl -s -F'file=@-' -Fexpires={{ expires_in }} https://0x0.st | tmux loadb -
    @tmux show-buffer

# Upload a file for 24h
[no-cd]
upload file expires_in="24":
    @curl -s -F'file=@{{ file }}' -Fexpires={{ expires_in }} https://0x0.st | tmux loadb -
    @tmux show-buffer

# Show cheat sheet for command
cheat cmd:
    curl cheat.sh/{{ cmd }}

# Tail a file
[no-cd]
tail log_file n="200":
    tail -{{ n }}f {{ log_file }}

# Grab a file/torrent
[no-cd]
grab target:
    aria2c -s 10 -x 10 {{ target }}

# Show ip
ip:
    ifconfig | awk '/192.168/ {print $2}' | sort -n | head -1

# Create a throwaway name
name *args:
    -@tname {{ args }}

# Example parallel usage
_parallel:
    #!/usr/bin/env -S parallel --shebang --ungroup --jobs {{ num_cpus() }}
    echo task 1 start; sleep 3; echo task 1 done
    echo task 2 start; sleep 3; echo task 2 done
    echo task 3 start; sleep 3; echo task 3 done
    echo task 4 start; sleep 3; echo task 4 done

