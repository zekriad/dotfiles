#!/usr/bin/env bash

# bash <(curl -L https://gitlab.com/zekriad/dotfiles/-/raw/main/bootstrap.sh)

if test -d /etc/nixos # NixOS?
then
  echo hi
#  sudo curl -fLo /etc/nixos/configuration.nix https://gist.github.com/zekriad/c4ddb39b7607bd1e12535e8958d11d9e/raw/bd6fadbdad8ee8b19002406ea63d0187b460b407/configuration.nix
#  sudo nixos-rebuild switch
fi

if ! command -v nix-env &> /dev/null
then
  sh <(curl -L https://nixos.org/nix/install) --daemon
fi

if ! test -e ~/.config/nixpkgs/config.nix &> /dev/null
then
  curl -fLo ~/.config/nixpkgs/config.nix --create-dirs https://gitlab.com/zekriad/dotfiles/-/raw/main/config/nixpkgs/config.nix
  nix-env --install zekdev

  git clone https://gitlab.com/zekriad/dotfiles.git ~/.dotfiles
  rm ~/.config/nixpkgs/config.nix
  rcup

  test -e ~/bin/z.sh || curl -fLo ~/bin/z.sh --create-dirs https://github.com/rupa/z/raw/master/z.sh
fi

# macOS
if [[ "$(uname)" = "Darwin" ]]
then
  # install homebrew & a few things from it
  export NONINTERACTIVE=1

  if ! command -v brew &> /dev/null
  then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  fi

  brew install zsh bash postgresql redis
  brew services start postgresql
  brew services start redis
  unset NONINTERACTIVE
fi

rustup default stable

# tmux plugin manager - <SPC>I
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# rbenv plugins
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
git clone https://github.com/jf/rbenv-gemset.git ~/.rbenv/plugins/rbenv-gemset
