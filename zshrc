command -v fastfetch > /dev/null && fastfetch

if test -e ~/.antigen.zsh &> /dev/null
then
  source ~/.antigen.zsh
  antigen use oh-my-zsh

  # Bundles from the default repo (robbyrussell's oh-my-zsh).
  # antigen bundle git
  # antigen bundle heroku
  # antigen bundle pip
  # antigen bundle lein
  # antigen bundle command-not-found

  # Syntax highlighting bundle.
  antigen bundle zsh-users/zsh-syntax-highlighting
  antigen bundle zsh-users/zsh-autosuggestions

  # Load the theme.
  # antigen theme frmendes/geometry

  # Tell Antigen that you're done.
  antigen apply
fi

# .local binaries
export PATH=~/.local/bin:$PATH

# Go
export PATH=/usr/local/opt/go/libexec/bin:$PATH
export GOPATH=~/go
export PATH=$GOPATH/bin:$PATH

# Playdate SDK
if test -e ~/Developer/PlaydateSDK &> /dev/null
then
  export export PLAYDATE_SDK_PATH=~/Developer/PlaydateSDK
fi

# Bun binaries
export PATH=~/.bun/bin:$PATH

# Rust binaries
export PATH=~/.cargo/bin:$PATH

# User bin
export PATH=~/bin:$PATH

# Current directory ./bin
export PATH=./bin:$PATH

export LANG=en_US.UTF-8
export HISTCONSTROL=ignoredups

# Don't clear screen after quitting man pages
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help"
export NAP_CONFIG=~/.config/nap/config.yml

if command -v nvim &> /dev/null
then
  export EDITOR="nvim"
  alias nv=nvim
fi

if command -v just &> /dev/null
 then
   export JUST_UNSTABLE=true
   alias j=just
fi

alias psg='ps aux | grep'

if command -v eza &> /dev/null
then
  alias ls="eza"
  alias la="ls -la"
  alias tree="eza --tree"
fi

if command -v git &> /dev/null
then
  alias gb='git branch -vv'
  alias gba='git branch -vv --all'
  alias lg="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
  alias gl="lg --stat"
  alias gc='git commit'
  alias gca='git commit -a'
  alias gco='git checkout'
  alias gs='git status -s'
  alias gsl='git stash list'
  alias gd='git diff'
  alias gdt='git difftool -g'
  alias gw='git wtf -l -a -r'
  alias gsu="git submodule update --init --recursive"
fi

if command -v tmux &> /dev/null
then
  alias tmux='tmux -2'
  alias t='tmux'
  alias ta='tmux attach-session'
  alias tkill='tmux kill-session'
fi


function pidof {
  ps -Ac | grep -i $argv | awk '{ print $1 }'
}

function napc {
  nap $1 | tee >(xsel -i --clipboard) | tmux load-buffer -
}

if command -v fzf &> /dev/null
then
  source <(fzf --zsh)
fi

alias whois="whois -h whois-servers.net"
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

# Canonical hex dump; some systems have this symlinked
command -v hd > /dev/null || alias hd="hexdump -C"

### macOS
if [[ "$(uname)" = "Darwin" ]] {
  function napc {
    nap $1 | tee >(pbcopy) | tmux load-buffer -
  }

  alias hidedesktop='defaults write com.apple.finder CreateDesktop -bool false && killall Finder'
  alias showdesktop='defaults write com.apple.finder CreateDesktop -bool true && killall Finder'

  # Clean up LaunchServices to remove duplicates in the “Open With” menu
  alias lscleanup="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user; killall Finder"

  test -e ~/.iterm2_shell_integration.zsh && source "${HOME}/.iterm2_shell_integration.zsh"
#   # Flush Directory Service cache
#   alias flush="dscacheutil -flushcache; killall -HUP mDNSResponder"

#   # OS X has no `md5sum`, so use `md5` as a fallback
#   command -v md5sum > /dev/null || alias md5sum="md5"
#   alias cleanup="find . -type f -name '*.DS_Store' -ls -delete"
}

# Fix for WSL until ~Creator's Update
if [[ "$(umask)" = "0000" ]] {
  umask 022
}

# Z directory jumping
test -e ~/.local/bin/z.sh && source ~/.local/bin/z.sh

# Just task runner completions - generate w just --completions zsh > ~/.just_completions.zsh
test -e ~/.just_completions.zsh && source ~/.just_completions.zsh

test -e ~/.local/bin/mise && eval "$(~/.local/bin/mise activate zsh)"

eval "$(starship init zsh)"

