class ItunesReceiptVerifier
  APP_STORE_SHARED_SECRET = 'd365f6749d1348c685117ec3ff067e9e'
  APPLE_RECEIPT_VERIFY_URL_SANDBOX = "https://sandbox.itunes.apple.com/verifyReceipt" # Sandbox
  APPLE_RECEIPT_VERIFY_URL_PRODUCTION = "https://buy.itunes.apple.com/verifyReceipt"   # Production

  #App Store error responses
  #21000 The App Store could not read the JSON object you provided.
  #21002 The data in the receipt-data property was malformed.
  #21003 The receipt could not be authenticated.
  #21004 The shared secret you provided does not match the shared secret on file for your account.
  #21005 The receipt server is not currently available.
  #21006 This receipt is valid but the subscription has expired.
  #21007 This receipt is a sandbox receipt, but it was sent to the production service for verification.
  #21008 This receipt is a production receipt, but it was sent to the sandbox service for verification.

  def self.verify_receipt_for(receipt_data, receipt_url)
    url = URI.parse(receipt_url)
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    json_request = {'receipt-data' => receipt_data, 'password' => APP_STORE_SHARED_SECRET}.to_json
    resp = http.post(url.path, json_request.to_s, {'Content-Type' => 'application/x-www-form-urlencoded'})

    if resp.code == '200'
      json_resp = JSON.parse(resp.body)
    end

    json_resp
  end

  def self.verify_receipt(receipt_data)
    json_resp = ItunesReceiptVerifier.verify_receipt_for(receipt_data, APPLE_RECEIPT_VERIFY_URL_PRODUCTION)

    if sandbox_receipt?
      json_resp = ItunesReceiptVerifier.verify_receipt_for(receipt_data, APPLE_RECEIPT_VERIFY_URL_SANDBOX)
    end

    json_resp
  end

  private

  def sandbox_receipt?
    status = response["status"] if response.kind_of? Hash
    status == 21007
  end
end
