module UserIdentifiable # Concerns without ActiveSupport
  include ActiveModel::Model

  def self.included(base_klass)
    base_klass.extend(ClassMethods)
    base_klass.class_eval do
      belongs_to :user
      validates :user_id, presence: true
    end
  end

  module ClassMethods
    def most_active_user
      User.find_by_id(select('count(id) as head_count, user_id').group('user_id').order('count(id) desc').first.user_id)
    end
  end

  def slug
    "#{self.class.name}_#{user_id}"
  end
end

