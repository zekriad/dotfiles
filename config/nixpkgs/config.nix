{
  allowUnfree = true;
  packageOverrides = pkgs: with pkgs; {
    zekdev = pkgs.buildEnv {
      name = "zekdev";
      paths = [
        # OS utils
        antigen
        aria2
        bat
        cheat
        exa
        fd
        fzf
        just
        neovim
        nmap
        rcm
        ripgrep
        socat
        tmux

        # dev utils
        aws
        delta
        editorconfig-core-c
        git
        git-lfs
        httpie
        k9s
        kubectx
        kubernetes
        terraform
        watchexec

        # languages
        nodejs
        rustup
        go
      ];
      pathsToLink = [ "/share" "/bin" "/Applications" ];
      extraOutputsToInstall = [ "man" "doc" ];
    };
  };
}

