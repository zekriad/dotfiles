# if test -e ~/.LESS__TERMCAP
    # . ~/.LESS_TERMCAP
# end

if not test -d ~/.rbenv
  git clone https://github.com/rbenv/rbenv.git ~/.rbenv
  cd ~/.rbenv; and src/configure; and make -C src
  git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
end

set -x PATH ~/.rbenv/bin $PATH
status --is-interactive; and source (rbenv init -|psub)

if not test -d ~/bin; mkdir ~/bin; end
set -x PATH ~/bin $PATH

# if not test -e ~/bin/exa
#   curl -L https://the.exa.website/releases/exa-0.4-linux-x86_64.zip | gunzip > ~/bin/exa
#   chmod +x ~/bin/exa
# end

# GOROOT path
set -x PATH /usr/local/opt/go/libexec/bin $PATH
# GOPATH
set -x GOPATH ~/go
set -x PATH $GOPATH/bin $PATH

if not test -d ~/.local/bin; mkdir --parents ~/.local/bin; end
set -x PATH ~/.local/bin $PATH

set -x PATH ~/.cargo/bin $PATH
set -x PATH ./bin $PATH

set -x LANG en_US.UTF-8
set -x HISTCONTROL ignoredups

# Don’t clear the screen after quitting a manual page
set -x MANPAGER "less -X"
# Highlight section titles in manual pages
# set -x LESS_TERMCAP_us \e\[1\x3B32m
set -x LESS_TERMCAP_md \e\[1\x3B31m

# Make some commands not show up in history
set -x HISTIGNORE "ls:cd:cd -:pwd:exit:date:* --help" # :ls *:

set -x EDITOR "emacs -nw"

# set -x CDPATH . /mnt/c/Users/zekri

alias in 'docker-compose run'
alias psg 'ps aux | grep'

alias ls "exa"
alias tree "exa --tree"
alias gb 'git branch -vv'
alias gba 'git branch -vv --all'
alias pyg 'pygmentize -f terminal256 -O style=monokai'
# alias gl='git log --decorate --graph --stat'
# alias lg='git log --oneline -n 15 --decorate --graph'
alias lg "git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gl "lg --stat"
alias gc 'git commit'
alias gca 'git commit -a'
alias gco 'git checkout'
alias gs 'git status -s'
alias gsl 'git stash list'
alias gd 'git diff'
alias gdt 'git difftool -g'
alias gw 'git wtf -l -a -r'
alias gsu "git submodule update --init --recursive"
alias tmux 'tmux -2'
alias t 'tmux'
alias tsc 'tmux switch-client -n'
alias ta 'tmux attach-session'
alias tkill 'tmux kill-session'

alias hidedesktop 'defaults write com.apple.finder CreateDesktop -bool false && killall Finder'
alias showdesktop 'defaults write com.apple.finder CreateDesktop -bool true && killall Finder'

function cless
  pyg $argv | less -R
end

function ccat
  pyg $argv
end

function pidof
  ps -Ac | grep -i $argv | awk '{ print $1 }'
end

alias whois "whois -h whois-servers.net"
alias sniff "sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump "sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

# Canonical hex dump; some systems have this symlinked
command -v hd > /dev/null or alias hd "hexdump -C"

alias rot13 'tr a-zA-Z n-za-mN-ZA-M'
alias urlencode 'python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1]);"'

# Python virtualenv
# source ~/dev/bin/activate

### macOS
if test (uname) = "Darwin"
  set -x o "open ."
  set -x oo "open .."

  # Clean up LaunchServices to remove duplicates in the “Open With” menu
  alias lscleanup "/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user; killall Finder"

  # Flush Directory Service cache
  alias flush "dscacheutil -flushcache; killall -HUP mDNSResponder"

  # OS X has no `md5sum`, so use `md5` as a fallback
  command -v md5sum > /dev/null or alias md5sum="md5"
  alias cleanup "find . -type f -name '*.DS_Store' -ls -delete"
end

starship init fish | source

# Fix for WSL until ~Creator's Update
if [ (umask) = 0000 ]; umask 022; end

# if not functions -q fisher
#  eval (curl -Lo ~/.config/fish/functions/fisher.fish --create-dirs git.io/fisher)

#  # plugins/themes
#  set -l pkgs z oh-my-fish/theme-chain
#  for pkg in $pkgs; echo $pkg >> ~/.config/fish/fishfile; end

#  set_color brcyan
#  echo "Run restart your shell and run `fisher`"
#  set_color normal
# end
