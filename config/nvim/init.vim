set nocompatible
filetype off

" Load vim-plug
if empty(glob("~/.local/share/nvim/site/autoload/plug.vim"))
  execute '!curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
endif

call plug#begin('~/.local/share/nvim/plugged')

"""" Color schemes
Plug 'morhetz/gruvbox'
Plug 'catppuccin/nvim', { 'as': 'catppuccin' }

"""" Languages
Plug 'sheerun/vim-polyglot'
Plug 'fatih/vim-go'
Plug 'tpope/vim-rails'
Plug 'chr4/nginx.vim'
Plug 'elixir-editors/vim-elixir'
Plug 'rust-lang/rust.vim'
Plug 'NoahTheDuke/vim-just'
Plug 'evanleck/vim-svelte', {'branch': 'main'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'} " :TSInstall go c ruby rust lua python javascript typescript elixir
Plug 'nvim-treesitter/nvim-treesitter-context'
Plug 'neovim/nvim-lspconfig'

"""" Tab Bar
Plug 'nvim-tree/nvim-web-devicons' " OPTIONAL: for file icons
Plug 'lewis6991/gitsigns.nvim' " OPTIONAL: for git status
Plug 'romgrk/barbar.nvim'

"""" Statusline
Plug 'itchyny/lightline.vim'
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'

"""" Tools
Plug 'janko-m/vim-test'
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'danro/rename.vim'
Plug 'airblade/vim-gitgutter'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mattn/emmet-vim'
Plug 'supermaven-inc/supermaven-nvim'
" Plug 'sbdchd/neoformat'

"""" General
"Plug 'cloudhead/neovim-fuzzy'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'dyng/ctrlsf.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'preservim/nerdtree'

"" Harpoon
Plug 'nvim-lua/plenary.nvim' " don't forget to add this one if you don't have it yet!
Plug 'ThePrimeagen/harpoon'

"""" end plugins
call plug#end()

" File ~/.vimrc
" Global settings for all files
set termguicolors
syntax enable
let g:gruvbox_contrast_dark = "medium"
let g:gruvbox_italic = 1
let ayucolor="dark"
"set background=dark
colorscheme catppuccin-mocha "gruvbox, catppuccin-latte, catppuccin macchiato, cappuccin-mocha

" trigger autocomplete on .
" au filetype go inoremap <buffer> . .<C-x><C-o>

set clipboard=unnamedplus
set number
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
set nowrap
set laststatus=2
set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P
set encoding=utf-8
set scrolloff=3
set showmode
set wildmenu
set wildmode=list:longest,full
set backspace=indent,eol,start
set ttyfast
set fileformat=unix
set fileformats=unix,dos
set splitright
set splitbelow
set ignorecase
set smartcase
"set spell spelllang=en_us

if executable('rg')
  " Use rg over grep
  set grepprg=rg\ --vimgrep
endif

" auto create directories on save
function! s:MkNonExDir(file, buf)
  if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
    let dir=fnamemodify(a:file, ':h')
    if !isdirectory(dir)
      call mkdir(dir, 'p')
    endif
  endif
endfunction

augroup BWCCreateDir
  autocmd!
  autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END
" end auto creating directories

" begin NeoFormat on save
"augroup fmt
"  autocmd!
"  autocmd BufWritePre * undojoin | Neoformat
"augroup END

"let g:neoformat_enabled_javascript = [ 'prettier' ]
"let g:neoformat_verbose = 1
" end NeoFormat on save

" being lightline settings
" let g:lightline = {'colorscheme': 'catppuccin_mocha'}
let g:lightline = {'colorscheme': 'rosepine'}

" begin vim-rust settings
let g:rustfmt_autosave = 1

" begin vim-go settings
let g:go_template_autocreate = 0
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'

au FileType go nmap <leader>gd <Plug>(go-def)
autocmd FileType go nmap <leader>gb<Plug>(go-build)
autocmd FileType go nmap <leader>gr<Plug>(go-run)
" end vim-go settings

" neovim-fuzzy settings
let g:fuzzy_opencmd = 'edit'

nnoremap <C-p> :FzfFiles<CR>
" end neovim-fuzzy settings

" begin vim-sexp settings
let g:sexp_enable_insert_mode_mappings = 0
" end vim-sexp settings

let mapleader = ","
" coc.nvim completion selection
inoremap <silent><expr> <Tab> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
" end coc.nvim completion selection

" zzz
nmap <leader>tn :set number!<CR>
nmap <leader>n :bnext<CR>
nmap <leader>p :bprevious<CR>
nmap <leader>d :bdelete<CR>
nmap <leader>wt :NERDTreeToggle<CR>
nmap <leader>wf :NERDTreeFind<CR>

let $FZF_DEFAULT_COMMAND = 'rg --files --no-messages'
let g:fzf_command_prefix = 'Fzf'
nnoremap <leader>v :FzfFiles<cr>

nmap     <C-F>f <Plug>CtrlSFPrompt
let g:ctrlsf_auto_close = {
    \ "normal" : 1,
    \ "compact": 1
    \}
let g:ctrlsf_auto_focus = {
    \ "at": "start"
    \ }
let g:ctrlsf_auto_preview = 1
let g:ctrlsf_default_root = 'project'
let g:ctrlsf_default_view_mode = 'compact'

" search settings
set gdefault
set incsearch
set showmatch
set hlsearch
noremap <leader><space> :noh<cr>
noremap <leader>cr :let @+=@%<cr>
noremap <leader>cc gg"+yG<cr>
nnoremap <tab> %
vnoremap <tab> %

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk

nnoremap <leader>w <C-w>
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

nnoremap <C-j> <C-d>
nnoremap <C-k> <C-u>

"" Harpoon
" Mark current file
nnoremap <C-a> :lua require("harpoon.mark").add_file()<CR>

" Open Harpoon menu
nnoremap <C-h> :lua require("harpoon.ui").toggle_quick_menu()<CR>

lua << EOF
require("lspconfig").gopls.setup{}
EOF

lua << EOF
require("nvim-treesitter.configs").setup {
  -- A list of parser names, or "all" (the listed parsers MUST always be installed)
  ensure_installed = { "go", "ruby", "rust", "javascript", "typescript", "c", "lua", "markdown", "markdown_inline" },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  -- List of parsers to ignore installing (or "all")
  ignore_install = { "javascript" },

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    disable = { "c", "rust" },
    -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
    disable = function(lang, buf)
        local max_filesize = 100 * 1024 -- 100 KB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
    end,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
EOF

lua << EOF
require("treesitter-context").setup{
  enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
  multiwindow = false, -- Enable multiwindow support.
  max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
  min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
  line_numbers = true,
  multiline_threshold = 20, -- Maximum number of lines to show for a single context
  trim_scope = 'outer', -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
  mode = 'cursor',  -- Line used to calculate context. Choices: 'cursor', 'topline'
  -- Separator between context and content. Should be a single character string, like '-'.
  -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
  separator = nil,
  zindex = 20, -- The Z-index of the context window
  on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
}
EOF

autocmd Filetype crontab setlocal nobackup nowritebackup
autocmd BufNewFile,BufRead *.zsh-theme set syntax=zsh
autocmd BufNewFile,BufRead Gemfile set syntax=ruby
autocmd BufNewFile,BufRead Podfile set syntax=ruby
autocmd BufNewFile,BufRead *.json set syntax=javascript

" fish shell stuff
autocmd Filetype fish compiler fish
autocmd Filetype fish setlocal textwidth=79
autocmd Filetype fish setlocal foldmethod=expr

" *****************************************

filetype plugin indent on "required!
"
" Brief help
"   :BundleList           - List configured bundles
"   :BundleInstall(!)     - install(update) bundles
"   :BundleSearch(!) foo  - search(or refresh cache first) for foo
"   :BundleClean(!)       - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..

" put in shell mode if fish launched vim
" if &shell =~# 'fish$'
"   set shell=zsh
" endif
